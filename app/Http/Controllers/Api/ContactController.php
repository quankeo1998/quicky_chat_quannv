<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function Symfony\Component\String\u;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        //
        $creator_id = Auth::id();
        $contacts = Contact::where('creator_id','=',$creator_id)
                            ->with('users') ;
        $keyword = '';
        if($request->get('keyword')){
            $keyword = $request->get('keyword');
            $contacts = $contacts->where('name','like',"%{$keyword}%");
        }
        $contacts = $contacts->get();
        return response()->json($contacts);
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $contact_data = [
            'user_id'=>$request->user_id,
            'name'=>$request->name, ///Chú ý: name request trùng với name trong db
            'creator_id'=>Auth::id(),
        ];
        Contact::create($contact_data);
        return response()->json([
            'status'=>true,
            'message'=>'Thêm thành công'
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show($user_id)
    {
        //
        $creator_id = Auth::id();
        $contact = Contact::where('creator_id','=',$creator_id)
                                    ->where('user_id','=',$user_id)
                                    ->with('users')->get();
        return response()->json($contact);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit()
    {
        //

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Contact $contact)
    {
        //
        $contact->delete();
        return response()->json([
            'status'=>true,
            'message'=>'Xóa thành công'
        ]);
    }
}
