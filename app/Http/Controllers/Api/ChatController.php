<?php

namespace App\Http\Controllers\Api;

use App\Events\SendMessage;
use App\Http\Controllers\Controller;
use App\Models\Conversation;
use App\Models\Message;
use App\Models\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Builder;

class ChatController extends Controller
{
    //Tạo cuộc trò chuyện nhóm
    public function storeGroup(Request $request)
    {

        $conversation = Conversation::create([
            'title'=>$request->title,
            'user_id'=>Auth::id(),
            'type'=>'group',
        ]);
        $user_id = $request->user_id;
        $participant_data[] = ['user_id'=>Auth::id()];
        foreach ($user_id as $value){
            $participant_data[] = ['user_id'=>$value];
        }
        $conversation?->participant()->createMany($participant_data);
            return response()->json([
            'status'=>true,
            'message'=>'Tạo cuộc trò chuyện thành công'
        ]);
    }

    //Thêm thành viên vào cuộc trò chuyện nhóm
    public function inviteConversation(Conversation $conversation,Request $request)
    {
        $user_id = $request->user_id;
        $user_data = [];
        foreach ($user_id as $value){
            $user_data[]=[
                'user_id'=>$value
            ];
        }
        $conversation?->participant()->createMany($user_data);
        return response()->json([
            'status'=>true,
            'message'=>'Thêm thành viên thành công'
        ]);
    }
    //Xóa thành viên cuộc trò chuyện
    public function removeMember(Conversation $conversation,$user_id)
    {
        if($conversation->user_id != Auth::id()){
            return response()->json([
                'status'=>false,
                'message'=>'Bạn không có quyền xóa thành viên'
            ],403);
        }
        $participant = $conversation->participant()->where('user_id',$user_id);
        $participant->delete();
        return response()->json([
            'status'=>true,
            'message'=>' Xóa thành viên thành công'
        ]);
    }


    //Lấy thông tin chi tiết cuộc trò chuyện
    public function editConversation($conversation_id)
    {
        $user = Auth::user();
        $conversation = $user->myConversation()->where('conversation_id','=',$conversation_id)->withCount('users')->with('users');
        $conversation = $conversation->get();
        return response()->json($conversation);
    }

    //Chỉnh sửa cuộc trò chuyện
    public function updateConversation(Conversation $conversation,Request $request)
    {
        $conversation->update([
            'title'=>$request->title
        ]);
        return response()->json([
            'status'=>true,
            'message'=>'Cập nhật thành công'
        ]);
    }

    //Xóa cuộc trò chuyện
    public function deleteConversation(Conversation $conversation)
    {
        $message = $conversation->messages();
        $message->delete();
        $participant=$conversation->participant();
        $participant->delete();
        $conversation->delete();
        return response()->json([
            'status'=>true,
            'message'=>'Xóa thành công'
        ]);
    }
    //Xóa nôi dung tin nhắn cuộc trò chuyện
    public function deleteMessageConversation(Conversation $conversation)
    {

        $conversation->messages()->delete();
        return response()->json([
            'status'=>true,
            'message'=>'Xóa thành công'
        ]);
    }

    //Tạo cuộc trò chuyện cá nhân
    public function storePrivate($user_id)
    {
        $conversation = Conversation::create([
            'user_id'=>Auth::id()
        ]);
        $participant_data = [
            [
                'user_id'=>Auth::id()
            ],
            [
                'user_id'=>$user_id
            ]
        ];
        $conversation?->participant()->createMany($participant_data);
        return response()->json([
            'status'=>true,
            'message'=>'Tạo cuộc trò chuyện thành công',
            'data'=>$conversation
        ]);
    }


    //Lưu tin nhắn
    public function storeMessage(Request $request,$conversation_id)
    {
        $message = Message::create([
            'content'=>$request->msg_content,
            'conversation_id'=>$conversation_id,
            'user_id'=>Auth::id()
        ]);
//        broadcast( new SendMessage(Auth::id(),$message,$conversation_id));
        return response()->json([
            'status'=>true,
            'message'=>'Thêm tin nhắn thành công'
        ]);
    }

    //Trả lời tin nhắn
    public function replyMessage(Request $request,$conversation_id,$message_id)
    {
        Message::create([
            'content'=>$request->msg_content,
            'conversation_id'=>$conversation_id,
            'user_id'=>Auth::id(),
            'parent_id'=>$message_id
        ]);
        return response()->json([
            'status'=>true,
            'message'=>'Thêm tin nhắn thành công'
        ]);
    }

    //Sửa tin nhắn
    public function updateMessage(Request $request, Message $message)
    {
        if($message->user_id != Auth::id()){
            return response()->json([
                'status'=>false,
                'message'=>'Bạn không có quyền sửa tin nhắn này'
            ],403);
        }
        $message->update([
            'content'=>$request->msg_content
        ]);
        return response()->json([
            'status'=>true,
            'message'=>'Sửa tin nhắn thành công'
        ]);
    }

    //Xóa tin nhắn của chính mình
    public function deleteMyMessage(Message $message)
    {
        if($message->user_id != Auth::id()){
            return response()->json([
                'status'=>false,
                'message'=>'Bạn không có quyền xóa tin nhắn này'
            ],403);
        }
        $message->delete();
        return response()->json([
            'status'=>true,
            'message'=>'Đã xóa tin nhắn thành công'
        ]);
    }

    //Xóa tin nhắn của user khác
    public function deleteUserMessage(Message $message)
    {
        $message->update([
            'status'=>0
        ]);
        return response()->json([
            'status'=>true,
            'message'=>'Đã xóa tin nhắn thành công'
        ]);
    }

    //Lấy danh sách cuộc trò chuyện
    public function getConversation()
    {
        $user = Auth::user();
        $conversations = $user->myConversation()->with('users')->withCount(['messages'=> function (Builder $query){
            $query->where('user_id','!=',Auth::id())
                  ->where('is_read','=',0);
        }])->with('messages',function ($message){
            $message->latest();
        });
        $conversations = $conversations->get();
        return response()->json($conversations);
    }
    //Lấy danh sách tin nhắn trong cuộc trò chuyện
    public function getMessage($conversation_id,Request $request)
    {
        $conversation = Conversation::find($conversation_id);

        $messages = $conversation->messages()->with('user')->oldest();
        $keyword = '';
        if($request->get('keyword')){
            $keyword = $request->get('keyword');
            $messages = $messages->where('content','like',"%{$keyword}%");
        }
        $messages = $messages->get();
        return response()->json($messages);
    }
    //Tải file ảnh
    public  function mediaUpload(Request $request){
        $file = $request->file('file');
        $path = $file->store('media','public');
        $image = [
            'disk'=>"public",
            'path'=>$path
        ];
        $image['path'] = Storage::disk($image['disk'])->url($image['path']);
        return response()->json($image);
    }
    //Cập nhật trạng thái xem tin nhắn

    public function updateMessageStatus(Conversation $conversation)
    {
        $user_id = Auth::id();
        $messages = $conversation->messages()->where('user_id','!=',$user_id)->get();
        foreach ($messages as $message){
            $message->update([
                "is_read"=>1
            ]);
    };
        return response()->json([
            'status'=>true,
            'message'=>'Đã cập nhật trạng thái xem thành công',
        ]);
    }
}
