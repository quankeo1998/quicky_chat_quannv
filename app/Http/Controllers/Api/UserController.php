<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return response()->json($user);
    }
    public function update(Request $request)
    {
        $user = Auth::user();
        $user->update([
            'name'=>$request->name,
            'phone_number'=>$request->phone_number,
            'image'=>$request->image,
            'address'=>$request->address,
            'birthday'=>$request->birthday
        ]);
        return response()->json([
            'status'=>true,
            'message'=> 'Cập nhật thành công',
            'data'=>$user
        ]);
    }
    //Tìm kiếm user theo tên
    public function search(Request $request)
    {
        $name = $request->get('keyword');
        $users = User::where('name','like',"%{$name}%")
                        ->get();
        return response()->json($users);
    }
}
