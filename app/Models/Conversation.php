<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Conversation extends Model
{
    use HasFactory,SoftDeletes;
    public $table ='conversations';
    protected $fillable =[
        'title',
        'user_id',
        'type',
    ];
    public function createBy(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function messages(){
        return $this->hasMany(Message::class,'conversation_id','id');
    }
    public  function participant(){
        return $this->hasMany(Participant::class,'conversation_id','id');
    }
    public function users(){
        return $this->belongsToMany(User::class,'participants','conversation_id','user_id');
    }
    public function getUserConversation(){
        return $this->belongsToMany(User::class,'participants','conversation_id','user_id')
            ->wherePivot('user_id','!=',Auth::id());
    }
    public function getUserSearchConversation($keyword){
        return $this->belongsToMany(User::class,'participants','conversation_id','user_id')
            ->wherePivot('user_id','!=',Auth::id())
            ->where('name','like',"%{$keyword}%");
    }
}
