<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use HasFactory,SoftDeletes;
    public $table = 'contacts';
    protected $fillable = [
        'user_id',
        'name',
        'creator_id'
    ];
    public function users(){
        return $this->belongsTo(User::class,'user_id','id');
    }

}
