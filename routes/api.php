<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
//

Route::group(['middleware'=>'auth:sanctum'],function (){
    //logout user
    Route::get('/auth/logout', [\App\Http\Controllers\Api\AuthController::class, 'logout'])->name('logout');
    //api contact
    Route::group(['prefix'=>'contacts'],function (){
        Route::get('/',[\App\Http\Controllers\Api\ContactController::class,'index']);
        Route::get('/user/{user_id}',[\App\Http\Controllers\Api\ContactController::class,'show']);
        Route::post('/update',[\App\Http\Controllers\Api\ContactController::class,'update']);
        Route::post('/create',[\App\Http\Controllers\Api\ContactController::class,'store']);
        Route::delete('/delete/{contact}',[\App\Http\Controllers\Api\ContactController::class,'destroy']);
        //search
        Route::get('/search',[\App\Http\Controllers\Api\ContactController::class,'index']);

    });
    // api user profile
    Route::group(['prefix'=>'user'],function (){
        Route::get('/',[\App\Http\Controllers\Api\UserController::class,'index']);
        Route::post('/update',[\App\Http\Controllers\Api\UserController::class,'update']);
        //search
        Route::get('/search',[\App\Http\Controllers\Api\UserController::class,'search']);
    });
    //api chat
    Route::group(['prefix'=>'conversations'],function (){
        //conversation
        Route::post('/store/{user_id}',[\App\Http\Controllers\Api\ChatController::class,'storePrivate']);
        Route::post('/store-group',[\App\Http\Controllers\Api\ChatController::class,'storeGroup']);
        Route::post('/invite/{conversation}',[\App\Http\Controllers\Api\ChatController::class,'inviteConversation']);
        Route::get('/',[\App\Http\Controllers\Api\ChatController::class,'getConversation']);
        Route::get('/{conversation_id}',[\App\Http\Controllers\Api\ChatController::class,'editConversation']);
        Route::put('/update/{conversation}',[\App\Http\Controllers\Api\ChatController::class,'updateConversation']);
        Route::post('/removeMember/{conversation}/{user_id}',[\App\Http\Controllers\Api\ChatController::class,'removeMember']);
        Route::delete('/delete/{conversation}',[\App\Http\Controllers\Api\ChatController::class,'deleteConversation']);
        //Xóa toàn bộ tin nhắn trong cuộc trò chuyện
        Route::delete('/delete-message/{conversation}',[\App\Http\Controllers\Api\ChatController::class,'deleteMessageConversation']);
        //message
        Route::post('/message/store/{conversation_id}',[\App\Http\Controllers\Api\ChatController::class,'storeMessage']);
        Route::get('/message/{conversation_id}',[\App\Http\Controllers\Api\ChatController::class,'getMessage']);
        Route::put('/message/{message}',[\App\Http\Controllers\Api\ChatController::class,'updateMessage']);
        //trả lời tin nhắn
        Route::post('/message/store/{conversation_id}/{message_id}',[\App\Http\Controllers\Api\ChatController::class,'replyMessage']);
        //xóa tin nhắn của chính mình
        Route::delete('/my-message/delete/{message}',[\App\Http\Controllers\Api\ChatController::class,'deleteMyMessage']);
        //xóa tin nhắn của user khác
        Route::put('/message-user/delete/{message}',[\App\Http\Controllers\Api\ChatController::class,'deleteUserMessage']);
        //Tìm kiếm tin nhắn
        Route::get('/message/{conversation_id}/search',[\App\Http\Controllers\Api\ChatController::class,'getMessage']);
        //Gửi tin nhắn kèm theo ảnh
        Route::get('/message/media/upload',[\App\Http\Controllers\Api\ChatController::class,'mediaUpload']);
        //Xác nhận tin nhắn đã xem
        Route::put('/message/read/{conversation}',[\App\Http\Controllers\Api\ChatController::class,'updateMessageStatus']);
    });
});
Route::post('/auth/register', [\App\Http\Controllers\Api\AuthController::class, 'signUp']);
Route::post('/auth/login', [\App\Http\Controllers\Api\AuthController::class, 'login'])->name('login');

